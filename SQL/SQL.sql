CREATE DATABASE mywebsite DEFAULT CHARACTER SET utf8;
USE mywebsite;

-- m_delivery_method

CREATE TABLE m_delivery_method (
    id int(11) PRIMARY KEY AUTO_INCREMENT,
    delivery_method varchar(256) NOT NULL,
    price int(11) NOT NULL
);

INSERT INTO m_delivery_method (id, delivery_method, price) VALUES
    (1, '通常配送', 0),
    (2, '日時指定配送', 300),
    (3, '速達配送', 500);
    

-- payment_method
CREATE TABLE payment_method (
    id int(11) PRIMARY KEY AUTO_INCREMENT,
    payment_method varchar(256) NOT NULL
);

INSERT INTO payment_method (id, payment_method) VALUES
    (1, 'クレジットカード'),
    (2, '電子マネー'),
    (3, 'コンビニ支払い'),
    (4, '代金引換');

-- t_user 
CREATE TABLE t_user (
    id int(11) PRIMARY KEY AUTO_INCREMENT, 
    name varchar(256) NOT NULL,
    address varchar(256) NOT NULL,
    login_id varchar(256) NOT NULL,
    login_password varchar(256) NOT NULL,
    create_date date NOT NULL
);

-- t_buy
CREATE TABLE t_buy (
    id int(11) PRIMARY KEY AUTO_INCREMENT,
    total_price int(11) NOT NULL,
    create_date date NOT NULL,
    user_id int(11) NOT NULL,
    delivery_method_id int(11) NOT NULL,
    payment_method_id int(11) NOT NULL
);

-- t_buy_detail
CREATE TABLE t_buy_detail (
    id int(11) PRIMARY KEY AUTO_INCREMENT,
    buy_id int(11) NOT NULL,
    item_id int(11) NOT NULL
);

-- m_item
CREATE TABLE m_item (
    id int(11) PRIMARY KEY AUTO_INCREMENT,
    name varchar(256) NOT NULL,
    detail text NOT NULL,
    price int(11) NOT NULL,
    file_name varchar(256) NOT NULL,
    item_detail_id int(11) NOT NULL
);

INSERT INTO m_item (id, name, detail, price, file_name, item_detail_id) VALUES
    (1, 'ワイシャツ カジュアル 無地 紺', '様々な季節で着ることが出来る一着となっております。Lサイズ', 4000, 'tops1.png', 1),
    (2, 'Tシャツ カットソー オーバーサイズ 緑', '秋、冬用で過ごしやすいTシャツです。胸元の文字がチャームポイントです。Mサイズ', 5000, 'tops2.png', 1),
    (3, 'ニット カジュアル 無地 赤', '冬用のニットで、着た時の肌触りが心地よい一着となっております。Mサイズ', 6000, 'tops3.png', 1),
    
    (4, 'ワイドパンツ カジュアル 無地 黒', 'どのような服装にも似合う黒のワイドパンツです。紐で調節できます。Mサイズ', 7000, 'pants1.png', 2),
    (5, 'ジーンズ カジュアル 無地', '普通のジーンズパンツよりもゆったりとしているのが特徴です。Mサイズ', 5000, 'pans2.png', 2),
    (6, 'スキニーパンツ カジュアル チェック柄 茶', '春から秋にかけて履けるスキニーです。無地のシャツにぴったりです。Sサイズ', 8000, 'pants3.png', 2),
    
    (7, 'MA-1 カジュアル 無地 紺', '肌寒い季節にぴったりのアウターです。Mサイズ', 9000, 'outerwear1.png', 3),
    (8, 'チェスターコート カジュアル 無地 茶', '冬の定番のコートです。どのような服装にもぴったりです。Lサイズ', 20000, 'outerwear2.png', 3),
    (9, 'ジャンパー カジュアル 無地 黒', '通気性が少なく体温を保てるため寒い季節にもってこいのアウターです。Mサイズ',100000, 'outerwear3.png', 3),
    
    (10, 'チャッカブーツ カジュアル オレンジ', '冬に人気なブーツですが、様々な季節でお使いいただけます。27cm', 10000, 'shoes1.png', 4),
    (11, '革靴 カジュアル 無地 黒', '無地なのでファッションはもちろん仕事面でも履くことができます。29cm', 30000, 'shoes2.png', 4),
    (12, 'スニーカー カジュアル 黒', '歩く負担を軽減してくれる液体ポケット付きで歩くことが楽しくなるような靴です。28cm', 15000, 'shoes3.png', 4),
    
    (13, 'キャップ 黒', '文字がおしゃれな帽子です。耐水性に優れ、雨の日でも水をはじくことができます。', 8000, 'other1.png', 5),
    (14, 'ネックレス', 'ダイアモンドが埋め込められており、光を当てると乱反射で輝きます。', 60000, 'other2.png', 5),
    (15, 'フープピアス', 'ピアス本体とピアスの留め具を一緒にすることにより留め具をなくすことがなくなります。', 3000, 'other3.png', 5);

-- m_item_detail
CREATE TABLE m_item_detail(
    id int(11) PRIMARY KEY AUTO_INCREMENT,
    type varchar(256) NOT NULL
);

INSERT INTO m_item_detail (id, type) VALUES
    (1, 'Tops'),
    (2, 'Pants'),
    (3, 'Outerwear'),
    (4, 'Shoes'),
    (5, 'Other');
    
UPDATE m_item SET 
    file_name = 'pants2.png' WHERE id = 5;
    
UPDATE m_item SET file_name = 'pants3.png' WHERE id = 6;

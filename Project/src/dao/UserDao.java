package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import base.DBManager;
import beans.UserDataBeans;
import project.ProjectHelper;

public class UserDao {
	//ログイン
//	public UserDataBeans getUserId(String loginId, String password) {
//		Connection conn = null;
//			try {
//				//データベースへ接続
//				conn = DBManager.getConnection();
//
//				//処理
//				//確認済のSQL
//				String sql = "SELECT * FROM t_user WHERE login_id = ? and login_password = ?";
//
//				//SELECTを実行し、結果表を取得
//				PreparedStatement pStmt = conn.prepareStatement(sql);
//				pStmt.setString(1, loginId);
//				pStmt.setString(2, ProjectHelper.password(password));
//				ResultSet rs = pStmt.executeQuery();
//
//				//ログイン失敗時
//				if(!rs.next()) {
//					return null;
//				}
//
//				//ログイン成功時
//				String loginIdData = rs.getString("login_id");
//				String passwordData = rs.getString("login_password");
//				return new UserDataBeans(loginIdData, passwordData);
//
//			}catch(SQLException e) {
//				e.printStackTrace();
//				return null;
//			}finally {
//				//データベース切断
//				if(conn != null) {
//					try {
//						conn.close();
//					}catch(SQLException e) {
//						e.printStackTrace();
//						return null;
//					}
//				}
//			}
//	}

	/**
	 * ユーザーIDを取得
	 *
	 * @param loginId
	 *            ログインID
	 * @param password
	 *            パスワード
	 * @return int ログインIDとパスワードが正しい場合対象のユーザーID 正しくない||登録されていない場合0
	 * @throws SQLException
	 *             呼び出し元にスロー
	 */
	public static int getUserId(String loginId, String password) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM t_user WHERE login_id = ?");
			st.setString(1, loginId);

			ResultSet rs = st.executeQuery();

			int userId = 0;
			while (rs.next()) {
				if (ProjectHelper.password(password).equals(rs.getString("login_password"))) {
					userId = rs.getInt("id");
					System.out.println("login succeeded");
					break;
				}
			}

			System.out.println("searching userId by loginId has been completed");
			return userId;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}
    //新規登録
    public static void insertUser(UserDataBeans udb) throws SQLException {
        Connection conn = null;

        try {
        	//データベースへ接続
        	conn = DBManager.getConnection();


        	 String sql = "INSERT INTO t_user (login_id, login_password, name, address, create_date) VALUES (?, ?, ?, ?, ?)";

        	 PreparedStatement pStmt = conn.prepareStatement(sql);

        	 pStmt.setString(1, udb.getLoginId());
        	 pStmt.setString(2, ProjectHelper.password(udb.getPassword()));
        	 pStmt.setString(3, udb.getName());
        	 pStmt.setString(4, udb.getAddress());
 			 pStmt.setTimestamp(5, new Timestamp(System.currentTimeMillis()));

        	 pStmt.executeUpdate();
 			System.out.println("inserting user has been completed");

        }catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (conn != null) {
				conn.close();
			}
		}


    }
	/**
	 * loginIdの重複チェック
	 *
	 * @param loginId
	 *            check対象のログインID
	 * @param userId
	 *            check対象から除外するuserID
	 * @return bool 重複している
	 * @throws SQLException
	 */
	public static boolean isOverlapLoginId(String loginId, int userId) throws SQLException {
		// 重複しているかどうか表す変数
		boolean isOverlap = false;
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			// 入力されたlogin_idが存在するか調べる
			st = con.prepareStatement("SELECT login_id FROM t_user WHERE login_id = ? AND id != ?");
			st.setString(1, loginId);
			st.setInt(2, userId);
			ResultSet rs = st.executeQuery();

			System.out.println("searching loginId by inputLoginId has been completed");

			if (rs.next()) {
				isOverlap = true;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}

		System.out.println("overlap check has been completed");
		return isOverlap;
	}

	//ユーザ情報を取得
	public static UserDataBeans getUserDataBeansByUserId(int userId) throws SQLException{
		UserDataBeans udb = new UserDataBeans();
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM t_user WHERE id =" + userId);
			ResultSet rs = st.executeQuery();

			while(rs.next()) {
				udb.setId(rs.getInt("id"));
				udb.setName(rs.getString("name"));
				udb.setAddress(rs.getString("address"));
				udb.setLoginId(rs.getString("login_id"));
			}
			st.close();

			}catch (SQLException e) {
				System.out.println(e.getMessage());
				throw new SQLException(e);
			}finally {
				if(con != null) {
					con.close();
				}
			}
			System.out.println("searching UserDataBeans by userId has been conpleted");
			return udb;
	}

	//ユーザ情報を更新する
	public static void updateUserData(UserDataBeans udb) throws SQLException{
		UserDataBeans updateUdb = new UserDataBeans();
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("UPDATE t_user SET login_id = ?, login_password = ?, name = ?, address = ? WHERE id = ?");
			st.setString(1, udb.getLoginId());
			st.setString(2, ProjectHelper.password(udb.getPassword()));
			st.setString(3, udb.getName());
			st.setString(4, udb.getAddress());
			st.setInt(5, udb.getId());
			st.executeUpdate();
			System.out.println("update has been completed");

			st = con.prepareStatement("SELECT * FROM t_user WHERE id =" + udb.getId());
			ResultSet rs = st.executeQuery();
			while(rs.next()) {
				updateUdb.setLoginId(rs.getString("login_id"));
				updateUdb.setPassword(rs.getString("login_password"));
				updateUdb.setName(rs.getString("name"));
				updateUdb.setAddress(rs.getString("address"));
				updateUdb.setId(rs.getInt("id"));
			}
			st.close();
			System.out.println("searching update-UserDataBeans has been completed");



		}catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if(con != null) {
				con.close();
			}
		}
	}

	//ユーザ情報を更新する
	public static void updateUserData2(UserDataBeans udb) throws SQLException{
		UserDataBeans updateUdb = new UserDataBeans();
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("UPDATE t_user SET login_id = ?, name = ?, address = ? WHERE id = ?");
			st.setString(1, udb.getLoginId());
			st.setString(2, udb.getName());
			st.setString(3, udb.getAddress());
			st.setInt(4, udb.getId());
			st.executeUpdate();
			System.out.println("update has been completed");

			st = con.prepareStatement("SELECT * FROM t_user WHERE id =" + udb.getId());
			ResultSet rs = st.executeQuery();
			while(rs.next()) {
				updateUdb.setLoginId(rs.getString("login_id"));
				updateUdb.setName(rs.getString("name"));
				updateUdb.setAddress(rs.getString("address"));
				updateUdb.setId(rs.getInt("id"));
			}
			st.close();
			System.out.println("searching update-UserDataBeans has been completed");



		}catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if(con != null) {
				con.close();
			}
		}
	}
}

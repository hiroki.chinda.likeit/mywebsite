package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.ItemDetailDataBeans;

public class ItemDetailDao {
	//配送方法の情報を取得
	public static ArrayList<ItemDetailDataBeans> getAllItemDetailDataBeans() throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM m_item_detail");
			ResultSet rs = st.executeQuery();

			ArrayList<ItemDetailDataBeans> itemDetailDataBeansList = new ArrayList<ItemDetailDataBeans>();
			while(rs.next()) {
				ItemDetailDataBeans iddb = new ItemDetailDataBeans();
				iddb.setId(rs.getInt("id"));
				iddb.setType(rs.getString("type"));
				itemDetailDataBeansList.add(iddb);
			}
			System.out.println("searching all ItemDetailDataBeans has been completed");

			return itemDetailDataBeansList;
		}catch (Exception e) {
			System.out.println(e.toString());
			throw new SQLException(e);
		}finally {
			if(con != null) {
				con.close();
			}
		}
	}
}

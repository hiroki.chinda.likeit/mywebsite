package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;

import base.DBManager;
import beans.BuyDataBeans;

public class BuyDao {

	//購入登録処理
	public static int insertBuy(BuyDataBeans bdb) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		int autoIncKey = -1;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"INSERT INTO t_buy(total_price, create_date, user_id, delivery_method_id, payment_method_id) VALUES(?,?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			st.setInt(1, bdb.getTotalPrice());
			st.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
			st.setInt(3, bdb.getUserId());
			st.setInt(4, bdb.getDeliveryMethodId());
			st.setInt(5, bdb.getPaymentMethodId());
			st.executeUpdate();

			ResultSet rs = st.getGeneratedKeys();
			if(rs.next()) {
				autoIncKey = rs.getInt(1);
			}
			System.out.println("inserting buy-datas has been completed");

			return autoIncKey;
		}catch(Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if(con != null) {
				con.close();
			}
		}
	}

	//buyIdから購入情報を検索
	public static BuyDataBeans getBuyDataBeansByBuyId(int buyId)throws SQLException{
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"SELECT * FROM t_buy "
							+"JOIN m_delivery_method "
							+"ON t_buy.delivery_method_id = m_delivery_method.id "
							+"JOIN payment_method "
							+"ON t_buy.payment_method_id = payment_method.id "
							+"WHERE t_buy.id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();
			BuyDataBeans bdb = new BuyDataBeans();
			if(rs.next()) {
				bdb.setId(rs.getInt("id"));
				bdb.setTotalPrice(rs.getInt("total_price"));
				bdb.setCreateDate(rs.getTimestamp("create_date"));
				bdb.setUserId(rs.getInt("user_id"));
				bdb.setDeliveryMethodId(rs.getInt("delivery_method_id"));
				bdb.setPaymentMethodId(rs.getInt("id"));
				bdb.setDeliveryMethodName(rs.getString("delivery_method"));
				bdb.setDeliveryMethodPrice(rs.getInt("price"));
				bdb.setPaymentMethodName(rs.getString("payment_method"));
			}

			System.out.println("searching BuyDataBeans by buyId has been completed");

			return bdb;
		}catch(Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if(con != null) {
				con.close();
			}
		}
	}

	//userIdから購入情報を検索
	public static ArrayList<BuyDataBeans> getBuyDataBeansByUserId(int userId)throws SQLException{
		Connection con = null;
		PreparedStatement st = null;

		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"SELECT * FROM t_buy "
							+"JOIN m_delivery_method "
							+"ON t_buy.delivery_method_id = m_delivery_method.id "
							+"JOIN payment_method "
							+"ON t_buy.payment_method_id = payment_method.id "
							+"WHERE t_buy.user_id = ?");
			st.setInt(1, userId);

			ResultSet rs = st.executeQuery();
			ArrayList<BuyDataBeans> BDBList = new ArrayList<BuyDataBeans>();
			while(rs.next()) {
				BuyDataBeans bdb = new BuyDataBeans();
				bdb.setId(rs.getInt("id"));
				bdb.setTotalPrice(rs.getInt("total_price"));
				bdb.setCreateDate(rs.getDate("create_date"));
				bdb.setUserId(rs.getInt("user_id"));
				bdb.setDeliveryMethodId(rs.getInt("delivery_method_id"));
				bdb.setPaymentMethodId(rs.getInt("id"));
				bdb.setDeliveryMethodName(rs.getString("delivery_method"));
				bdb.setDeliveryMethodPrice(rs.getInt("price"));
				bdb.setPaymentMethodName(rs.getString("payment_method"));
				BDBList.add(bdb);
			}

			System.out.println("searching BuyDataBeans by buyId has been completed");

			return BDBList;
		}catch(Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if(con != null) {
				con.close();
			}
		}
	}


}

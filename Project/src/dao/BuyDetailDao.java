package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import base.DBManager;
import beans.BuyDetailDataBeans;
import beans.ItemDataBeans;

public class BuyDetailDao{

	//購入した商品を表示
	public static List<ItemDataBeans> getItemList() throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"SELECT * FROM t_buy_detail t "
							+"INNER JOIN m_item m "
							+"ON t.item_id = m.id");
			ResultSet rs = st.executeQuery();
			List<ItemDataBeans> IDBList = new ArrayList<ItemDataBeans>();
			while(rs.next()) {
				ItemDataBeans idb = new ItemDataBeans();

				idb.setId(rs.getInt("id"));
				idb.setBuyId(rs.getInt("buy_id"));
				idb.setName(rs.getString("name"));
				idb.setPrice(rs.getInt("price"));


				IDBList.add(idb);
			}
			return IDBList;

		}catch(Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if(con != null) {
				con.close();
			}
		}
	}


	//購入情報登録
	public static void insertBuyDetail(BuyDetailDataBeans bddb) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO t_buy_detail(buy_id, item_id) VALUES (?,?)");
			st.setInt(1, bddb.getBuyId());
			st.setInt(2, bddb.getItemId());
			st.executeUpdate();
			System.out.println("inserting BuyDetail has been completed");


		}catch(Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if(con != null) {
				con.close();
			}
		}
	}

	//buyIdから購入情報検索
	public ArrayList<BuyDetailDataBeans> getBuyDataBeansByBuyId(int buyId) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM t_buy_detail WHERE id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();
			ArrayList<BuyDetailDataBeans> bddbList = new ArrayList<BuyDetailDataBeans>();

			while(rs.next()) {
				BuyDetailDataBeans bddb = new BuyDetailDataBeans();

				bddb.setId(rs.getInt("id"));
				bddb.setBuyId(rs.getInt("user_id"));
				bddb.setItemId(rs.getInt("item_id"));
				bddbList.add(bddb);
			}
			System.out.println("searching BuyDataBeansList by buyId has beeb completed");
			return bddbList;
		}catch(Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if(con != null) {
				con.close();
			}
		}
	}

	//buyIdから購入情報詳細検索
	public static ArrayList<ItemDataBeans> getItemDataBeansListByBuyId(int buyId) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"SELECT m_item.id, m_item.name, m_item.price "
								+"FROM t_buy_detail "
								+"JOIN m_item "
								+"ON t_buy_detail.item_id = m_item.id "
								+"WHERE t_buy_detail.buy_id = ?");
			st.setInt(1, buyId);

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> IDBList = new ArrayList<ItemDataBeans>();

			while(rs.next()) {
				ItemDataBeans IDB = new ItemDataBeans();
				IDB.setId(rs.getInt("id"));
				IDB.setName(rs.getString("name"));
				IDB.setPrice(rs.getInt("price"));
				IDBList.add(IDB);
			}

			System.out.println("searching ItemDataBeansList by buyId has been completed");

			return IDBList;
		}catch(Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if(con != null) {
				con.close();
			}
		}
	}

	//userIdから購入情報詳細検索
	public static ArrayList<ItemDataBeans> getItemDataBeansListByUserId(int userId) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement(
					"SELECT m_item.id, m_item.name, m_item.price "
								+"FROM t_buy_detail "
								+"JOIN m_item "
								+"ON t_buy_detail.item_id = m_item.id "
								+"WHERE t_buy_detail.buy_id = ?");
			st.setInt(1, userId);

			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> IDBList = new ArrayList<ItemDataBeans>();

			while(rs.next()) {
				ItemDataBeans IDB = new ItemDataBeans();
				IDB.setId(rs.getInt("id"));
				IDB.setName(rs.getString("name"));
				IDB.setPrice(rs.getInt("price"));
				IDBList.add(IDB);
			}

			System.out.println("searching ItemDataBeansList by buyId has been completed");

			return IDBList;
		}catch(Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if(con != null) {
				con.close();
			}
		}
	}
}

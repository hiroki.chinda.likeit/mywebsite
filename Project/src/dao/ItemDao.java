package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.ItemDataBeans;

public class ItemDao {

	/**
	 * ランダムで引数指定分のItemDataBeansを取得
	 * @param limit 取得したいかず
	 * @return <ItemDataBeans>
	 * @throws SQLException
	 */
	public static ArrayList<ItemDataBeans> getRandItem(int limit) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM m_item ORDER BY RAND() LIMIT ? ");
			st.setInt(1, limit);

			ResultSet rs = st.executeQuery();

			ArrayList<ItemDataBeans> itemList = new ArrayList<ItemDataBeans>();

			while (rs.next()) {
				ItemDataBeans item = new ItemDataBeans();
				item.setId(rs.getInt("id"));
				item.setName(rs.getString("name"));
				item.setDetail(rs.getString("detail"));
				item.setPrice(rs.getInt("price"));
				item.setFileName(rs.getString("file_name"));
				item.setItemDetailId(rs.getInt("item_detail_id"));
				itemList.add(item);
			}
			System.out.println("getAllItem completed");
			return itemList;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		} finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//商品の情報を取得
	public static ItemDataBeans getItemDetailByItemId(int itemId) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM m_item WHERE id = ?");
			st.setInt(1, itemId);

			ResultSet rs = st.executeQuery();

			ItemDataBeans idb = new ItemDataBeans();
			if(rs.next()) {
				idb.setId(rs.getInt("id"));
				idb.setName(rs.getString("name"));
				idb.setDetail(rs.getString("detail"));
				idb.setPrice(rs.getInt("price"));
				idb.setFileName(rs.getString("file_name"));
				idb.setItemDetailId(rs.getInt("item_detail_id"));
			}

			System.out.println("searching item by itemId has been conpleted");

			return idb;
		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if (con != null) {
				con.close();
			}
		}
	}

	//商品名から商品の情報を取得
	public static ArrayList<ItemDataBeans> getItemByItemName(String searchWord) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			if(searchWord.length() == 0) {
				st = con.prepareStatement("SELECT * FROM m_item");
			}else {
				st = con.prepareStatement("SELECT * FROM m_item WHERE name LIKE ?");
				st.setString(1, "%"+ searchWord +"%");
			}
			ResultSet rs = st.executeQuery();
			ArrayList<ItemDataBeans> idbList = new ArrayList<ItemDataBeans>();
			while(rs.next()) {
				ItemDataBeans idb = new ItemDataBeans();
				idb.setId(rs.getInt("id"));
				idb.setName(rs.getString("name"));
				idb.setDetail(rs.getString("detail"));
				idb.setPrice(rs.getInt("price"));
				idb.setFileName(rs.getString("file_name"));
				idb.setItemDetailId(rs.getInt("item_detail_id"));
				idbList.add(idb);
			}

			System.out.println("searching Item by ItemName has been completed");
			return idbList;
		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if(con != null) {
				con.close();
			}
		}
	}

	//商品数を取得
	public static double getItemCount(String searchWord) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT count(*) as cnt FROM m_item WHERE name LIKE ?");
			st.setString(1, "%"+ searchWord +"%");
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while(rs.next()) {
				coung =Double.parseDouble(rs.getString("cnt"));
			}

			return coung;

		}catch(Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if(con != null) {
				con.close();
			}
		}
	}

	//商品更新
	public static void updateItemData(ItemDataBeans item)throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			ItemDataBeans updateIDB = new ItemDataBeans();
			st = con.prepareStatement("UPDATE m_item SET file_name = ?, name = ?, price = ?, detail = ? WHERE id = ?");
			st.setString(1, item.getFileName());
			st.setString(2, item.getName());
			st.setInt(3, item.getPrice());
			st.setString(4, item.getDetail());
			st.setInt(5, item.getId());
			st.executeUpdate();
			System.out.println("update has been completed");
			System.out.println(item.getId());

			st = con.prepareStatement("SELECT * FROM m_item WHERE id =" + item.getId());
			ResultSet rs = st.executeQuery();
			while(rs.next()) {
				updateIDB.setFileName(rs.getString("file_name"));
				updateIDB.setName(rs.getString("name"));
				updateIDB.setPrice(rs.getInt("price"));
				updateIDB.setDetail(rs.getString("detail"));
				updateIDB.setId(rs.getInt("id"));
			}
			st.close();
			System.out.println("searching update-ItemData has been completed");

		}catch(Exception e) {
			System.out.println(e.toString());
			throw new SQLException(e);
		}finally {
			if(con != null) {
				con.close();
			}
		}
	}

	//商品を追加
	public static void insertItem(ItemDataBeans idb) throws SQLException {
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("INSERT INTO m_item (file_name, name, price, detail, item_detail_id) VALUES (?,?,?,?,?)");
			st.setString(1, idb.getFileName());
			st.setString(2, idb.getName());
			st.setInt(3, idb.getPrice());
			st.setString(4, idb.getDetail());
			st.setInt(5, idb.getItemDetailId());

			st.executeUpdate();

			System.out.println("inserting item has been completed");
		}catch(SQLException e) {
			System.out.println(e.toString());
			throw new SQLException(e);
		}finally {
			if(con != null) {
				con.close();
			}
		}
	}

	//商品を削除
	public static void itemDelete(String itemId) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("DELETE FROM m_item WHERE id = ?");
			st.setString(1, itemId);
			st.executeUpdate();

		}catch(SQLException e) {
			System.out.println(e.toString());
			throw new SQLException(e);
		}finally {
			if(con != null) {
				con.close();
			}
		}
	}

	//商品カテゴリから商品の情報を取得
	public static ArrayList<ItemDataBeans> getItemByItemCategory(int itemDetailId) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();

			st = con.prepareStatement("SELECT * FROM m_item WHERE item_detail_id = ?");
			st.setInt(1, itemDetailId);
			ResultSet rs = st.executeQuery();

			ArrayList<ItemDataBeans> idbList = new ArrayList<ItemDataBeans>();
			while(rs.next()) {
				ItemDataBeans idb = new ItemDataBeans();
				idb.setId(rs.getInt("id"));
				idb.setName(rs.getString("name"));
				idb.setDetail(rs.getString("detail"));
				idb.setPrice(rs.getInt("price"));
				idb.setFileName(rs.getString("file_name"));
				idb.setItemDetailId(rs.getInt("item_detail_id"));
				idbList.add(idb);
			}

			System.out.println("searching Item by ItemCategory has been completed");
			return idbList;
		}catch(SQLException e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if(con != null) {
				con.close();
			}
		}
	}

	//商品数を取得
	public static double getItemCountByItemCategory(int itemDetailId) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT count(*) as cnt FROM m_item WHERE item_detail_id = ?");
			st.setInt(1, itemDetailId);
			ResultSet rs = st.executeQuery();
			double coung = 0.0;
			while(rs.next()) {
				coung =Double.parseDouble(rs.getString("cnt"));
			}

			return coung;

		}catch(Exception e) {
			System.out.println(e.getMessage());
			throw new SQLException(e);
		}finally {
			if(con != null) {
				con.close();
			}
		}
	}
}

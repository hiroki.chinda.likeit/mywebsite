package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import base.DBManager;
import beans.PaymentMethodBeans;

public class PaymentMethodDao {
	public static ArrayList<PaymentMethodBeans> getAllPaymentMethodBeans() throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM payment_method");
			ResultSet rs = st.executeQuery();

			ArrayList<PaymentMethodBeans> paymentMethodBeansList = new ArrayList<PaymentMethodBeans>();
			while(rs.next()) {
				PaymentMethodBeans pmb = new PaymentMethodBeans();
				pmb.setId(rs.getInt("id"));
				pmb.setPaymentMethod(rs.getString("payment_method"));
				paymentMethodBeansList.add(pmb);
			}

			System.out.println("searching all PaymentMethodBeans has been completed");

			return paymentMethodBeansList;
		}catch (Exception e) {
			System.out.println(e.toString());
			throw new SQLException(e);
		}finally {
			if(con != null) {
				con.close();
			}
		}
	}

	public static PaymentMethodBeans getPaymentMethodBeansByID(int PaymentMethodId) throws SQLException{
		Connection con = null;
		PreparedStatement st = null;
		try {
			con = DBManager.getConnection();
			st = con.prepareStatement("SELECT * FROM payment_method WHERE id = ?");
			st.setInt(1, PaymentMethodId);

			ResultSet rs = st.executeQuery();

			PaymentMethodBeans pmb = new PaymentMethodBeans();
			if(rs.next()) {
				pmb.setId(rs.getInt("id"));
				pmb.setPaymentMethod(rs.getString("payment_method"));
			}

			System.out.println("searching PaymentMethodBeans by PaymentMethodId has been completed");

			return pmb;

		}catch (Exception e) {
			System.out.println(e.toString());
			throw new SQLException(e);
		}finally {
			if(con != null) {
				con.close();
			}
		}
	}
}

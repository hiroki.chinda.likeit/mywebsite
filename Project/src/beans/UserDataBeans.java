package beans;

import java.io.Serializable;

public class UserDataBeans implements Serializable{
	private int id;
	private String name;
	private String address;
	private String loginId;
	private String password;

	public UserDataBeans(String loginIdData, String nameData) {
		// TODO 自動生成されたコンストラクター・スタブ
	}
	public UserDataBeans() {
		// TODO 自動生成されたコンストラクター・スタブ
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getLoginId() {
		return loginId;
	}
	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * ユーザー情報更新時の必要情報をまとめてセットするための処理
	 *
	 * @param name
	 * @param loginId
	 * @param address
	 * @param
	 */
	public void setUpdateUserDataBeansInfo(String name, String password, String loginId, String address, int id) {
		this.name = name;
		this.password = password;
		this.loginId = loginId;
		this.address = address;
		this.id = id;
	}

}

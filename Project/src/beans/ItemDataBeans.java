package beans;

import java.io.Serializable;

public class ItemDataBeans implements Serializable{
	private int id;
	private String name;
	private String detail;
	private int price;
	private String fileName;
	private int itemDetailId;
	private int buyId;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public int getItemDetailId() {
		return itemDetailId;
	}
	public void setItemDetailId(int itemDetailId) {
		this.itemDetailId = itemDetailId;
	}
	public String getFormatPrice() {
		return String.format("%,d", this.price);
	}
	public int getBuyId() {
		return buyId;
	}
	public void setBuyId(int buyId) {
		this.buyId = buyId;
	}

	//商品更新時に使用
	public void setUpdateItemDataBeansInfo(String fileName, String name, int price, String detail, int id) {
		this.fileName = fileName;
		this.name = name;
		this.price = price;
		this.detail = detail;
		this.id = id;
	}
}

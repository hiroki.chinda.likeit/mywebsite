package beans;

import java.io.Serializable;

public class PaymentMethodBeans implements Serializable{
	private int id;
	private String paymentMethod;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
}

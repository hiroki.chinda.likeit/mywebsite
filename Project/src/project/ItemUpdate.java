package project;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.ItemDataBeans;
import dao.ItemDao;

/**
 * Servlet implementation class ItemUpdate
 */
@WebServlet("/ItemUpdate")
@MultipartConfig(location = "C:\\Users\\LIKEIT_STUDENT.DESKTOP-DK4DFCO\\Documents\\mywebsite\\Project\\WebContent\\img", maxFileSize = 1048576)
public class ItemUpdate extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemUpdate() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//セッションにユーザの情報をセット
		HttpSession session = request.getSession();
		try {
			// itemIDをパラメータから取得
			int itemId = Integer.parseInt(request.getParameter("item_id"));
			System.out.println(itemId);

			// 更新確認画面から戻ってきた場合Sessionから取得。それ以外はuserIdでユーザーを取得
			ItemDataBeans item = (ItemDataBeans) ItemDao.getItemDetailByItemId(itemId);

			request.setAttribute("item", item);


			request.getRequestDispatcher(ProjectHelper.ITEM_UPDATE_PAGE).forward(request, response);

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}

	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
			try{

				Part part = request.getPart("file_name_update");
				String name = this.getFileName(part);
				part.write(name);

				ItemDataBeans item = new ItemDataBeans();
				item.setUpdateItemDataBeansInfo(name,request.getParameter("name_update"),Integer.parseInt(request.getParameter("price_update")),request.getParameter("detail_update"), Integer.parseInt(request.getParameter("item_id")));


				// 登録が確定されたかどうか確認するための変数
				String confirmed = request.getParameter("confirm_button");
				switch (confirmed) {
				case "cancel":
					request.setAttribute("returnIDB", item);
					//response.sendRedirect("ItemDetail");
					request.getRequestDispatcher(ProjectHelper.ITEM_PAGE).forward(request, response);
					break;

				case "update":
					ItemDao.updateItemData(item);
					request.setAttribute("item", item);
					//response.sendRedirect("ItemDetail");
					request.getRequestDispatcher(ProjectHelper.ITEM_PAGE).forward(request, response);
					break;
				}



		}catch (Exception e) {
		e.printStackTrace();
		session.setAttribute("errorMessage", e.toString());
		response.sendRedirect("Error");
		}
	}

	private String getFileName(Part part) {
        String name = null;
        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
            if (dispotion.trim().startsWith("filename")) {
                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
                name = name.substring(name.lastIndexOf("\\") + 1);
                break;
            }
        }
        return name;
	}

}

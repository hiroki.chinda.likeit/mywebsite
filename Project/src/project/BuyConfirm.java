package project;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BuyDataBeans;
import beans.DeliveryMethodDataBeans;
import beans.ItemDataBeans;
import beans.PaymentMethodBeans;
import dao.DeliveryMethodDao;
import dao.PaymentMethodDao;

/**
 * Servlet implementation class BuyConfirm
 */
@WebServlet("/BuyConfirm")
public class BuyConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			//配送方法のidをint型に型変換し、取得
			int inputDeliveryMethodId = Integer.parseInt(request.getParameter("delivery_method_id"));
			//配送方法の情報をidを基に取得
			DeliveryMethodDataBeans userSelectDMDB = DeliveryMethodDao.getDeliveryMethodDataBeansByID(inputDeliveryMethodId);

			//決済方法のidをint型に型変換し、取得
			int inputPaymentMethodId = Integer.parseInt(request.getParameter("payment_method_id"));
			//決済方法の情報をidを基に取得
			PaymentMethodBeans userSelectPMB = PaymentMethodDao.getPaymentMethodBeansByID(inputPaymentMethodId);

			ArrayList<ItemDataBeans> cartIDBList = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

			//ProjectHelperで商品の合計と配送料金を足す
			int totalPrice = ProjectHelper.getTotalItemPrice(cartIDBList);
			totalPrice += userSelectDMDB.getPrice();

			BuyDataBeans bdb = new BuyDataBeans();
			bdb.setUserId((int) session.getAttribute("userId"));
			bdb.setTotalPrice(totalPrice);
			bdb.setDeliveryMethodId(userSelectDMDB.getId());
			bdb.setDeliveryMethodName(userSelectDMDB.getDeliveryMethod());
			bdb.setDeliveryMethodPrice(userSelectDMDB.getPrice());
			bdb.setPaymentMethodId(userSelectPMB.getId());
			bdb.setPaymentMethodName(userSelectPMB.getPaymentMethod());

			session.setAttribute("bdb", bdb);
			request.getRequestDispatcher(ProjectHelper.BUY_CONFIRM_PAGE).forward(request, response);
		}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errMsg", e.toString());
			response.sendRedirect("Error");
		}
	}

}

package project;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import beans.UserDataBeans;
import dao.ItemDao;
import dao.UserDao;

/**
 * Servlet implementation class ItemDetail
 */
@WebServlet("/ItemDetail")
public class ItemDetail extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			//選択された商品のIDを型変換し利用
			int id = Integer.parseInt(request.getParameter("item_id"));
//			String sId = request.getParameter("item_id");
//			if(sId!=null) {
//				int id = Integer.parseInt(sId);
//			}else {
//
//			}

			//対象のアイテム情報を取得
			ItemDataBeans item = ItemDao.getItemDetailByItemId(id);
			//リクエストパラメーターにセット
			request.setAttribute("item", item);

			//ログインユーザが管理者か確認
			int userId = (int) session.getAttribute("userId");
			UserDataBeans udb = (UserDataBeans) UserDao.getUserDataBeansByUserId(userId);
			request.setAttribute("udb", udb);

			request.getRequestDispatcher(ProjectHelper.ITEM_PAGE).forward(request, response);
		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errorMessage", e.toString());
			response.sendRedirect("Error");
		}
	}

}

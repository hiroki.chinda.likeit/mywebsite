package project;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDao;

/**
 * Servlet implementation class UserDataUpdateConfirm
 */
@WebServlet("/UserDataUpdateConfirm")
public class UserDataUpdateConfirm extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDataUpdateConfirm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
			try{
				String inputLoginId = request.getParameter("login_id");
				String inputPassword = request.getParameter("password");
				String inputConfirmPassword = request.getParameter("confirm_password");
				String inputUserName = request.getParameter("user_name");
				String inputUserAddress = request.getParameter("user_address");

				UserDataBeans udb = new UserDataBeans();
				udb.setName(inputUserName);
				udb.setLoginId(inputLoginId);
				udb.setPassword(inputPassword);
				udb.setPassword(inputConfirmPassword);
				udb.setAddress(inputUserAddress);

				//エラーメッセージを格納する変数
				String validationMessage = "";

			udb.setUpdateUserDataBeansInfo(request.getParameter("name_update"),request.getParameter("password"),request.getParameter("login_id_update"),request.getParameter("address_update"), (int) session.getAttribute("userId"));

			//パスワードとパスワード（確認）が一致しているかをチェック

			if (!inputPassword.equals(inputConfirmPassword)) {
				validationMessage += "入力されているパスワードと確認用パスワードが違います<br>";
			}

			// ログインIDの入力規則チェック 英数字 ハイフン アンダースコアのみ入力可能
			if (!ProjectHelper.isLoginIdValidation(udb.getLoginId())) {
				validationMessage += "半角英数とハイフン、アンダースコアのみ入力できます";
			}
			// loginIdの重複をチェック
			if (UserDao.isOverlapLoginId(udb.getLoginId(), 0)) {
				validationMessage += "ほかのユーザーが使用中のログインIDです";
			}

			//バリデーションエラーメッセージがないなら確認画面へ
			if(validationMessage.length() == 0){
				request.setAttribute("udb", udb);
				request.getRequestDispatcher(ProjectHelper.USER_DATA_UPDATE_CONFIRM_PAGE).forward(request, response);
			}else {

				//セッションにエラーメッセージを持たせてユーザー画面へ
				session.setAttribute("validationMessage", validationMessage);
				response.sendRedirect("UserData");
			}


		}catch (Exception e) {
		e.printStackTrace();
		session.setAttribute("errorMessage", e.toString());
		response.sendRedirect("Error");
		}
	}

}

package project;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDao;

/**
 * Servlet implementation class ItemAdd
 */
@WebServlet("/ItemAdd")
public class ItemAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemAdd() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			//item_idをint型へ型変換
			int id = Integer.parseInt(request.getParameter("item_id"));
			//商品情報を取得
			ItemDataBeans idb = ItemDao.getItemDetailByItemId(id);

			request.setAttribute("idb", idb);

			//カートを取得
			ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

			//カートがない場合作成
			if(cart == null) {
				cart = new ArrayList<ItemDataBeans>();
			}

			//カートに商品を追加
			cart.add(idb);

			session.setAttribute("cart", cart);
			request.setAttribute("cartActionMessage", "カートに商品を追加しました");
			request.getRequestDispatcher(ProjectHelper.CART_PAGE).forward(request, response);
		}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errMsg", e.toString());
			response.sendRedirect("errMsg");
		}
	}

}

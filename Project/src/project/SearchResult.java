package project;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;
import dao.ItemDao;

/**
 * Servlet implementation class SearchResult
 */
@WebServlet("/SearchResult")
public class SearchResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SearchResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			String searchWord = request.getParameter("search_word");
			request.setAttribute("searchWord", searchWord);

			ArrayList<ItemDataBeans> searchResultIDBList = ItemDao.getItemByItemName(searchWord);
			double itemCount = ItemDao.getItemCount(searchWord);

			request.setAttribute("itemCount", (int) itemCount);
			request.setAttribute("itemList", searchResultIDBList);

			request.getRequestDispatcher(ProjectHelper.SEARCH_RESULT_PAGE).forward(request, response);

		}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errMsg", e.toString());
			response.sendRedirect("Error");
		}
	}



}

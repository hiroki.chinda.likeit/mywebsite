package project;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.sql.Connection;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import beans.ItemDataBeans;

public class ProjectHelper {
		// 検索結果
		static final String SEARCH_RESULT_PAGE = "/WEB-INF/jsp/searchresult.jsp";
		// 商品ページ
		static final String ITEM_PAGE = "/WEB-INF/jsp/itemdetail.jsp";
		//商品編集ページ
		static final String ITEM_UPDATE_PAGE = "/WEB-INF/jsp/itemupdate.jsp";
		//商品新規登録ページ
		static final String ITEM_REGIST_PAGE = "/WEB-INF/jsp/newitem.jsp";
		// TOPページ
		static final String TOP_PAGE = "/WEB-INF/jsp/index.jsp";
		// エラーページ
		static final String ERROR_PAGE = "/WEB-INF/jsp/error.jsp";
		// 買い物かごページ
		static final String CART_PAGE = "/WEB-INF/jsp/cart.jsp";
		// 購入
		static final String BUY_PAGE = "/WEB-INF/jsp/buy.jsp";
		// 購入確認
		static final String BUY_CONFIRM_PAGE = "/WEB-INF/jsp/buyconfirm.jsp";
		// 購入完了
		static final String BUY_RESULT_PAGE = "/WEB-INF/jsp/buyresult.jsp";
		// ユーザー情報
		static final String USER_DATA_PAGE = "/WEB-INF/jsp/userdata.jsp";
		//ユーザ情報更新
		static final String USER_DATA_UPDATE_PAGE = "/WEB-INF/jsp/userdataupdate.jsp";
		// ユーザー情報更新確認
		static final String USER_DATA_UPDATE_CONFIRM_PAGE = "/WEB-INF/jsp/userdataupdateconfirm.jsp";
		// ユーザー情報更新完了
		static final String USER_DATA_UPDATE_RESULT_PAGE = "/WEB-INF/jsp/userdataupdateresult.jsp";
		// ユーザー購入履歴
		static final String BUY_HISTORY_PAGE = "/WEB-INF/jsp/buyhistory.jsp";
		// ログイン
		static final String LOGIN_PAGE = "/WEB-INF/jsp/login.jsp";
		// ログアウト
		static final String LOGOUT_PAGE = "/WEB-INF/jsp/logout.jsp";
		// 新規登録
		static final String REGIST_PAGE = "/WEB-INF/jsp/regist.jsp";
		// 新規登録入力内容確認
		static final String REGIST_CONFIRM_PAGE = "/WEB-INF/jsp/registconfirm.jsp";
		// 新規登録完了
		static final String REGIST_RESULT_PAGE = "/WEB-INF/jsp/registresult.jsp";

		public static ProjectHelper getInstance() {
			return new ProjectHelper();
		}


		/**
		 * 商品の合計金額を算出する
		 *
		 * @param items
		 * @return total
		 */
		public static int getTotalItemPrice(ArrayList<ItemDataBeans> items) {
			int total = 0;
			for (ItemDataBeans item : items) {
				total += item.getPrice();
			}

			return total;
		}

		/**
		 * セッションから指定データを取得（削除も一緒に行う）
		 *
		 * @param session
		 * @param str
		 * @return
		 */
		public static Object cutSessionAttribute(HttpSession session, String str) {
			Object test = session.getAttribute(str);
			session.removeAttribute(str);

			return test;
		}

		//暗号化
	    public static String password(String password) {
	    	Connection con = null;
	        try {
	        	String source = password;

	        	Charset charset = StandardCharsets.UTF_8;

	        	String algorithm = "MD5";

	        	byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
	        	String result = DatatypeConverter.printHexBinary(bytes);

	        	System.out.println(result);

	        	return result;
	        }catch (Exception e) {
	            e.printStackTrace();
	            return null;

	        }


	    }
		/**
		 * ログインIDのバリデーション
		 *
		 * @param inputLoginId
		 * @return
		 */
		public static boolean isLoginIdValidation(String inputLoginId) {
			// 英数字アンダースコア以外が入力されていたら
			if (inputLoginId.matches("[0-9a-zA-Z-_]+")) {
				return true;
			}

			return false;

		}
}

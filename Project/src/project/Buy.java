package project;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.DeliveryMethodDataBeans;
import beans.ItemDataBeans;
import beans.PaymentMethodBeans;
import dao.DeliveryMethodDao;
import dao.PaymentMethodDao;

/**
 * Servlet implementation class Buy
 */
@WebServlet("/Buy")
public class Buy extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Buy() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		try {
			Boolean isLogin = session.getAttribute("isLogin") != null ? (Boolean) session.getAttribute("isLogin") : false;
			ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");

			if(!isLogin) {
				session.setAttribute("returnStrUrl", "Buy");
				response.sendRedirect("Login");
			}else if(cart.size() == 0){
				request.setAttribute("cartActionMessage", "カートに商品がありません");
				request.getRequestDispatcher(ProjectHelper.CART_PAGE).forward(request, response);
			}else {
				ArrayList<PaymentMethodBeans> pMBList = PaymentMethodDao.getAllPaymentMethodBeans();
				ArrayList<DeliveryMethodDataBeans> dMDBList = DeliveryMethodDao.getAllDeliveryMethodDataBeans();
				request.setAttribute("pmbList", pMBList);
				request.setAttribute("dmdbList", dMDBList);
				request.getRequestDispatcher(ProjectHelper.BUY_PAGE).forward(request, response);
			}
		}catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errMsg", e.toString());
			response.sendRedirect("Error");
		}
	}



}

package project;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import beans.ItemDataBeans;
import beans.ItemDetailDataBeans;
import dao.ItemDao;
import dao.ItemDetailDao;

/**
 * Servlet implementation class ItemRegist
 */
@WebServlet("/ItemRegist")
@MultipartConfig(location = "C:\\Users\\LIKEIT_STUDENT.DESKTOP-DK4DFCO\\Documents\\mywebsite\\Project\\WebContent\\img", maxFileSize = 1048576)
public class ItemRegist extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ItemRegist() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ItemDataBeans idb = new ItemDataBeans();
		ArrayList<ItemDetailDataBeans> iDDBList = null;
		try {
			iDDBList = ItemDetailDao.getAllItemDetailDataBeans();
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		request.setAttribute("iDDBList", iDDBList);
		request.getRequestDispatcher(ProjectHelper.ITEM_REGIST_PAGE).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		try {

			Part part = request.getPart("file_name");
			String name = this.getFileName(part);
			part.write(name);

			String inputName = request.getParameter("name");
			int inputPrice = Integer.parseInt(request.getParameter("price"));
			String inputDetail = request.getParameter("detail");
			int inputItemDetailId = Integer.parseInt(request.getParameter("item_detail_id"));

			ItemDataBeans idb = new ItemDataBeans();

			idb.setFileName(name);
			idb.setName(inputName);
			idb.setPrice(inputPrice);
			idb.setDetail(inputDetail);
			idb.setItemDetailId(inputItemDetailId);

			ItemDao.insertItem(idb);
			request.setAttribute("idb", idb);
			response.sendRedirect("Index");

		} catch (Exception e) {
			e.printStackTrace();
			session.setAttribute("errMsg", e.toString());
			response.sendRedirect("Error");
		}
	}

	private String getFileName(Part part) {
        String name = null;
        for (String dispotion : part.getHeader("Content-Disposition").split(";")) {
            if (dispotion.trim().startsWith("filename")) {
                name = dispotion.substring(dispotion.indexOf("=") + 1).replace("\"", "").trim();
                name = name.substring(name.lastIndexOf("\\") + 1);
                break;
            }
        }
        return name;
	}
}

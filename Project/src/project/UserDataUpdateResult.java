package project;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDao;

/**
 * Servlet implementation class UserDataUpdateResult
 */
@WebServlet("/UserDataUpdateResult")
public class UserDataUpdateResult extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDataUpdateResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
			try{
				UserDataBeans udb = new UserDataBeans();
				udb.setUpdateUserDataBeansInfo(request.getParameter("name_update_confirm"),request.getParameter("password_update_confirm"),request.getParameter("login_id_update_confirm"),request.getParameter("address_update_confirm"), (int) session.getAttribute("userId"));


				// 登録が確定されたかどうか確認するための変数
				String confirmed = request.getParameter("confirm_button");
				switch (confirmed) {
				case "cancel":
					session.setAttribute("returnUDB", udb);
					response.sendRedirect("UserData");
					break;

				case "update":
					//パスワードが入力された場合とそうでない場合
					if(request.getParameter("password_update_confirm").equals("")) {
						UserDao.updateUserData2(udb);
					}else {
						UserDao.updateUserData(udb);
					}
					request.setAttribute("udb", udb);
					request.getRequestDispatcher(ProjectHelper.USER_DATA_UPDATE_RESULT_PAGE).forward(request, response);
					break;
				}



		}catch (Exception e) {
		e.printStackTrace();
		session.setAttribute("errorMessage", e.toString());
		response.sendRedirect("Error");
		}
	}

}

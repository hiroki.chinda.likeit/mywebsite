package project;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemDataBeans;

/**
 * Servlet implementation class ItemDelete
 */
@WebServlet("/ItemDelete")
public class ItemDelete extends HttpServlet {
	private static final long serialVersionUID = 1L;




	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		HttpSession session = request.getSession();
		try {
			String index = request.getParameter("index");
			ArrayList<ItemDataBeans> cart = (ArrayList<ItemDataBeans>) session.getAttribute("cart");
			int itemIdInt = Integer.parseInt(index);
			for(int i = cart.size();i>=0;i--) {
				if(i == itemIdInt) {
					cart.remove(cart.get(i));
				}
			}

			session.setAttribute("cart", cart);
			request.getRequestDispatcher(ProjectHelper.CART_PAGE).forward(request, response);
		}catch(Exception e) {
			e.printStackTrace();
			session.setAttribute("errMsg", e.toString());
			response.sendRedirect("Error");
		}

	}

}

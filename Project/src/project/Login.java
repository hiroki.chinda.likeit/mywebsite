package project;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserDataBeans;
import dao.UserDao;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

//		//リクエストパラメータの入力項目を取得
//		String loginId = request.getParameter("login_id");
//		String password = request.getParameter("password");
//
//		//上記を引数に渡して、Dao実行
//		UserDao userDao = new UserDao();
//		UserDataBeans user = userDao.getUserId(loginId, password);
//
//		//ログイン失敗時
//		if(user == null) {
//			request.setAttribute("loginErrMsg", "ログインIDまたはパスワードが異なります");
//			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
//			dispatcher.forward(request,  response);
//			return;
//		}
//
//		//セッションにユーザの情報をセット
//		HttpSession session = request.getSession();
//		session.setAttribute("userInfo", user);
//
//
//		response.sendRedirect("UserListServlet");
	}

}

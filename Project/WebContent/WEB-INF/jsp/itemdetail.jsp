<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>商品詳細画面</title>
    <link href="css/style.css" rel="styleSheet" type="text/css" />
    <link rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous">
        <!-- Place your kit's code here -->
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
</head>

<body>
    <div class="wrapper">
        <header>
            <h1 class="headline">
                <a href="Index" style="text-decoration: none;" class="title">Fashion EC</a>
            </h1>
            <ul class="nav-list">
                <li class="nav-list-item"><a href="UserData"><i class="fas fa-user">ユーザ情報</i></a></li>
                <li class="nav-list-item"><a href="Cart"><i class="fas fa-shopping-cart">カート</i></a></li>
                <li class="nav-list-item"><a href="Logout"><i class="fas fa-sign-out-alt">ログアウト</i></a></li>
            </ul>
        </header>

        <p class="sample_h_4">商品詳細</p>
                <span class="btn-haichi2">
                    <div class="col s12">
                        <div class="btn-modoru">
                            <p class="center-align">
                                <a href="SearchResult?search_word=${searchWord}" class="btn-square-pop" style="text-decoration: none;">検索一覧へ戻る</a>
                            </p>
                        </div>
                    </div>

	                    <div class="col s12">
	                        <div class="btn-touroku">
	                        	<form action="ItemAdd" method="POST">
	                            	<p class="center-align">
		                            	<input type="hidden" name="item_id" value="${item.id}">
		                                <button class="btn-square-pop" type="submit" name="action">カートに保存</button>
	                            	</p>
	                            </form>
	                        </div>
	                    </div>
                </span>
        <div class="itemdetail-contents">
			<div class="row">
				<div class="col s6">
					<div class="card">
						<div class="card-image">
                            <img src="img/${item.fileName}" alt="イラスト1" width="679" height="607">
						</div>
					</div>
				</div>
				<div class="col s6">
                  <div class="itemdetail-table">
                    <table width="1000">
                     <tr>
                      <th>
                        <div class="itemdetail-name">
                            <h1>商品名</h1>
                        </div>
                      </th>
                      <td>
                        <div>
                            <h1>${item.name}</h1>
                        </div>
                    　 </td>
                     </tr>
                     <tr>
                      <th>
                        <div class="itemdetail-money">
                            <h1>値段</h1>
                        </div>
                      </th>
                      <td>
                          <h1>${item.formatPrice}円</h1>
                    　 </td>
                     </tr>
                    </table>
                  </div>
                    <font style="padding:6px 50px; background:#248; color:#ffffff; font-weight:bold; font-size: 30px;">商品詳細</font>
                    <div style="border:1px solid #248; padding:10px; font-size:1.2em; margin-top:2px;">${item.detail}</div>
				</div>
			</div>
        </div>
			<c:if test="${udb.id == 1}">
                <span class="btn-haichi2">
                    <div class="col s12">
                        <div class="btn-modoru">
                        <form action="ItemUpdate">
                            <p class="center-align">
		                        <input type="hidden" name="item_id" value="${item.id}">
                                <button class="btn-square-pop" type="submit" name="?">編集</button>
                            </p>
                        </form>
                        </div>
                    </div>
                    <div class="col s12">
                        <div class="btn-touroku">
                        <form action="ItemMasterDelete" method="POST">
                            <p class="center-align">
		                        <input type="hidden" name="item_id" value="${item.id}">
                                <button class="btn-square-pop" type="submit" name="?">削除</button>
                            </p>
                        </form>
                        </div>
                    </div>
                </span>
			</c:if>

    <footer>
        <nav id="footer-nav">
        <ul>
            <li class=”current”><a href="SearchByCategory?item_detail_id=1" name="item_detail_id">Tops</a></li>
            <li><a href="SearchByCategory?item_detail_id=2" name="item_detail_id">Pants</a></li>
            <li><a href="SearchByCategory?item_detail_id=3" name="item_detail_id">Outerwear</a></li>
            <li><a href="SearchByCategory?item_detail_id=4" name="item_detail_id">Shoes</a></li>
            <li><a href="SearchByCategory?item_detail_id=5" name="item_detail_id">Other</a></li>
        </ul>
        </nav>
        <p>© All rights reserved by Fashion EC.</p>
    </footer>

    </div>




</body>

</html>
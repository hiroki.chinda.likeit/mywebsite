<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>検索結果画面</title>
    <link href="css/style.css" rel="styleSheet" type="text/css" />
    <link rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous">
        <!-- Place your kit's code here -->
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
</head>

<body>
    <div class="wrapper">
        <header>
            <h1 class="headline">
                <a href="Index" style="text-decoration: none;" class="title">Fashion EC</a>
            </h1>
            <ul class="nav-list">
                <li class="nav-list-item"><a href="UserData"><i class="fas fa-user">ユーザ情報</i></a></li>
                <li class="nav-list-item"><a href="Cart"><i class="fas fa-shopping-cart">カート</i></a></li>
                <li class="nav-list-item"><a href="Logout"><i class="fas fa-sign-out-alt">ログアウト</i></a></li>
            </ul>
        </header>

    <p class="sample_h_4">検索結果</p>

    <form class="form-wrapper cf" action="SearchResult">
        <input type="text" name="search_word" placeholder="Search here...">
        <button type="submit">検索</button>
    </form>

    <p style="text-align: center;">
    	<c:if test="${empty searchWord}"></c:if>
    	<c:if test="${not empty searchWord}">検索ワード：${searchWord}</c:if>
    </p>

    <p style="text-align: center;">
	    <c:if test="${itemCount == 0}">
	    見つかりませんでした
	    </c:if>
	    <c:if test="${itemCount != 0}">
	    ${itemCount}件見つかりました
	    </c:if>
    </p>

    <c:forEach var="itemList" items="${itemList}">
	    <div class="box22">
	        <table width="100%">
		        <tr>
		            <td align="center"><a href="ItemDetail?item_id=${itemList.id}"><img src="img/${itemList.fileName}" width="100" height="100"></a></td>
		            <td align="center" style="font-size: 40px;">${itemList.name}</td>
		            <td align="center" style="font-size: 40px;">${itemList.formatPrice}円</td>
		        </tr>
	        </table>
	    </div>
    </c:forEach>

<!--     <div class="pager">
      <ul class="pagination justify-content-center">
          <li class="pre"><a href="#"><span>«</span></a></li>
          <li><a href="#" class="active"><span>1</span></a></li>
          <li><a href="#"><span>2</span></a></li>
          <li><a href="#"><span>3</span></a></li>
          <li><a href="#"><span>4</span></a></li>
          <li><a href="#"><span>5</span></a></li>
          <li class="next"><a href="#"><span>»</span></a></li>
      </ul>
    </div>
-->


    <footer>
        <nav id="footer-nav">
        <ul>
            <li class=”current”><a href="SearchByCategory?item_detail_id=1" name="item_detail_id">Tops</a></li>
            <li><a href="SearchByCategory?item_detail_id=2" name="item_detail_id">Pants</a></li>
            <li><a href="SearchByCategory?item_detail_id=3" name="item_detail_id">Outerwear</a></li>
            <li><a href="SearchByCategory?item_detail_id=4" name="item_detail_id">Shoes</a></li>
            <li><a href="SearchByCategory?item_detail_id=5" name="item_detail_id">Other</a></li>
        </ul>
        </nav>
        <p>© All rights reserved by Fashion EC.</p>
    </footer>

    </div>




</body>

</html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>カート画面</title>
    <link href="css/style.css" rel="styleSheet" type="text/css" />
    <link rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous">
        <!-- Place your kit's code here -->
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
</head>

<body>
    <div class="wrapper">
        <header>
            <h1 class="headline">
                <a href="Index" style="text-decoration: none;" class="title">Fashion EC</a>
            </h1>
            <ul class="nav-list">
                <li class="nav-list-item"><a href="UserData"><i class="fas fa-user">ユーザ情報</i></a></li>
                <li class="nav-list-item"><a href="Cart"><i class="fas fa-shopping-cart">カート</i></a></li>
                <li class="nav-list-item"><a href="Logout"><i class="fas fa-sign-out-alt">ログアウト</i></a></li>
            </ul>
        </header>
    <p class="sample_h_4">カート</p>
    <div align="center">
    	${cartActionMessage}
    </div>
    <div class="">
            <div class="col s12">
                <div class="btn-haichi-cart">
                    <a href="Buy" class="btn-square-pop" type="submit">購入画面へ</a>
                </div>
            </div>
    </div>


	<c:forEach var="item" items="${cart}" varStatus="status">
	    <div class="box22">
	        <table width="100%">
	        <tr>
	            <td align="center" width="400"><img src="img/${item.fileName}" width="100" height="100"></td>
	            <td align="center" style="font-size: 25px;" width="500" height="100">${item.name}</td>
	            <td align="center" style="font-size: 25px;" width="250">${item.formatPrice}円</td>
	            <td align="center" style="font-size: 25px;" width="200">
	                <form action="ItemDelete" method="POST">
	                	<input type="hidden" value="${status.index}" name="index">
	                    <button type="submit" name="action">削除</button>
	                </form>
	        </tr>
	        </table>
	    </div>
    </c:forEach>

    <footer>
        <nav id="footer-nav">
        <ul>
            <li class=”current”><a href="SearchByCategory?item_detail_id=1" name="item_detail_id">Tops</a></li>
            <li><a href="SearchByCategory?item_detail_id=2" name="item_detail_id">Pants</a></li>
            <li><a href="SearchByCategory?item_detail_id=3" name="item_detail_id">Outerwear</a></li>
            <li><a href="SearchByCategory?item_detail_id=4" name="item_detail_id">Shoes</a></li>
            <li><a href="SearchByCategory?item_detail_id=5" name="item_detail_id">Other</a></li>
        </ul>
        </nav>
        <p>© All rights reserved by Fashion EC.</p>
    </footer>

    </div>




</body>

</html>
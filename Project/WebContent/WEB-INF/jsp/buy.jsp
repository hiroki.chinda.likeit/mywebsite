<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>購入画面</title>
    <link href="css/style.css" rel="styleSheet" type="text/css" />
    <link rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous">
        <!-- Place your kit's code here -->
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
</head>

<body>
    <div class="wrapper">
        <header>
            <h1 class="headline">
                <a href="Index" style="text-decoration: none;" class="title">Fashion EC</a>
            </h1>
            <ul class="nav-list">
                <li class="nav-list-item"><a href="UserData"><i class="fas fa-user">ユーザ情報</i></a></li>
                <li class="nav-list-item"><a href="Cart"><i class="fas fa-shopping-cart">カート</i></a></li>
                <li class="nav-list-item"><a href="Logout"><i class="fas fa-sign-out-alt">ログアウト</i></a></li>
            </ul>
        </header>

        <div class="box26">
        <span class="box-title">購入情報登録</span>
            <h2>決済方法と配送方法を選択してください</h2>
            <form action="BuyConfirm" method="POST">
            <div class="cp_ipselect">
                <select class="cp_sl06" name="payment_method_id" required>
                    <option value="" hidden disabled selected></option>
                    <c:forEach var="pmb" items="${pmbList}">
                    	<option value="${pmb.id}">${pmb.paymentMethod}</option>
                    </c:forEach>
                </select>
                <span class="cp_sl06_highlight"></span>
                <span class="cp_sl06_selectbar"></span>
                <label class="cp_sl06_selectlabel">決済方法</label>
            </div>
                <div class="cp_ipselect">
                <select class="cp_sl06" name="delivery_method_id" required>
                    <option value="" hidden disabled selected></option>
                    <c:forEach var="dmdb" items="${dmdbList}">
                    	<option value="${dmdb.id}">${dmdb.deliveryMethod}</option>
                    </c:forEach>
                </select>
                <span class="cp_sl06_highlight"></span>
                <span class="cp_sl06_selectbar"></span>
                <label class="cp_sl06_selectlabel">配送方法</label>
            </div>

            <div class="table-buy">
                <table>
                  <tr>
                    <th width="50%">商品名</th>
                    <th width="20%">値段</th>
                    <th width="40%">小計</th>
                  </tr>
                <c:forEach var="cartInItem" items="${cart}">
                  <tr>
                    <td>${cartInItem.name}</td>
                    <td>${cartInItem.formatPrice}</td>
                    <td>${cartInItem.formatPrice}円</td>
                  </tr>
                </c:forEach>
                </table>
            </div>
            <div class="btn-buyconfirm">
                <div class="btn-buyconfirm2">
                        <div class="col s12">
                            <div class="btn-modoru-buyconfirm">
                                <a class="btn-square-pop" href="Cart">戻る</a>
                            </div>
                        </div>
                </div>
                <div class="btn-buyconfirm2">
                        <div class="col s12">
                            <div class="btn-haichi-buyconfirm">
                                <button class="btn-square-pop" type="submit" name="?">確定</button>
                            </div>
                        </div>
                </div>
            </div>
            </form>
    </div>
    <footer>
        <nav id="footer-nav">
        <ul>
            <li class=”current”><a href="SearchByCategory?item_detail_id=1" name="item_detail_id">Tops</a></li>
            <li><a href="SearchByCategory?item_detail_id=2" name="item_detail_id">Pants</a></li>
            <li><a href="SearchByCategory?item_detail_id=3" name="item_detail_id">Outerwear</a></li>
            <li><a href="SearchByCategory?item_detail_id=4" name="item_detail_id">Shoes</a></li>
            <li><a href="SearchByCategory?item_detail_id=5" name="item_detail_id">Other</a></li>
        </ul>
        </nav>
        <p>© All rights reserved by Fashion EC.</p>
    </footer>

    </div>




</body>

</html>
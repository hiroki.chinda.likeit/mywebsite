<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>新規登録完了画面</title>
    <link href="css/style.css" rel="styleSheet" type="text/css" />
    <link rel="stylesheet"
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
      crossorigin="anonymous">
        <!-- Place your kit's code here -->
    <link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">
</head>

<body>
    <div class="wrapper">
        <header>
            <h1 class="headline">
                <a href="Index" style="text-decoration: none;" class="title">Fashion EC</a>
            </h1>
            <ul class="nav-list">
                <li class="nav-list-item"><a href="UserData"><i class="fas fa-user">ユーザ情報</i></a></li>
                <li class="nav-list-item"><a href="Cart"><i class="fas fa-shopping-cart">カート</i></a></li>
                <li class="nav-list-item"><a href="Logout"><i class="fas fa-sign-out-alt">ログアウト</i></a></li>
            </ul>
        </header>

        <p class="sample_h_4">新規登録完了</p>
        <div class="box">
            <form action="Index" method="post">
                <div class="cp_iptxt">
                    <input class="ef2" type="text" value="${udb.loginId}" readonly>
                    <label>ログインID</label>
                    <span class="focus_line"></span>
                </div>
                <div class="cp_iptxt">
                    <input class="ef2" type="password" value="${udb.password}" readonly>
                    <label>パスワード</label>
                    <span class="focus_line"></span>
                </div>
                <div class="cp_iptxt">
                    <input class="ef2" type="password" value="${udb.password}" readonly>
                    <label>パスワード確認</label>
                    <span class="focus_line"></span>
                </div>
                <div class="cp_iptxt">
                    <input class="ef2" type="text" value="${udb.name}" readonly>
                    <label>ユーザ名</label>
                    <span class="focus_line"></span>
                </div>
                <div class="cp_iptxt">
                    <input class="ef2" type="text"value="${udb.address}" readonly>
                    <label>住所</label>
                    <span class="focus_line"></span>
                </div>
                <div class="registOK">
                    <p>上記の内容で登録しました！</p>
                </div>
                <div class="col s12">
                    <div class="btn-haichi">
                        <p class="center-align">
                            <a class="btn-square-pop" type="submit" href="Login">ログイン画面へ</a>
                        </p>
                    </div>
                </div>
            </form>
        </div>


    <footer>
        <nav id="footer-nav">
        <ul>
            <li class=”current”><a href="SearchByCategory?item_detail_id=1" name="item_detail_id">Tops</a></li>
            <li><a href="SearchByCategory?item_detail_id=2" name="item_detail_id">Pants</a></li>
            <li><a href="SearchByCategory?item_detail_id=3" name="item_detail_id">Outerwear</a></li>
            <li><a href="SearchByCategory?item_detail_id=4" name="item_detail_id">Shoes</a></li>
            <li><a href="SearchByCategory?item_detail_id=5" name="item_detail_id">Other</a></li>
        </ul>
        </nav>
        <p>© All rights reserved by Fashion EC.</p>
    </footer>

    </div>




</body>

</html>
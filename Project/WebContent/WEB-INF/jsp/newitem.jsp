<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>商品登録</title>
<link href="css/style.css" rel="styleSheet" type="text/css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- Place your kit's code here -->
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	rel="stylesheet">
</head>

<body>
	<div class="wrapper">
		<header>
			<h1 class="headline">
				<a href="Index" style="text-decoration: none;" class="title">Fashion
					EC</a>
			</h1>
			<ul class="nav-list">
				<li class="nav-list-item"><a href="UserData"><i
						class="fas fa-user">ユーザ情報</i></a></li>
				<li class="nav-list-item"><a href="Cart"><i
						class="fas fa-shopping-cart">カート</i></a></li>
				<li class="nav-list-item"><a href="Logout"><i
						class="fas fa-sign-out-alt">ログアウト</i></a></li>
			</ul>
		</header>
		<p class="sample_h_4">商品新規登録</p>
		<form action="ItemRegist" method="POST" enctype="multipart/form-data">
			<div class="box">
				<div class="cp_iptxt">
					<img src="/*javascriptで作成*/"> <input class="ef" type="file"
						name="file_name" value="${idb.fileName}">
				</div>
				<div class="cp_ipselect">
                <select class="cp_sl06" name="item_detail_id" required>
	                <option value="${idb.itemDetailId}" hidden disabled selected></option>
	                    <c:forEach var="iddb" items="${iDDBList}">
	                    	<option value="${iddb.id}">${iddb.type}</option>
	                    </c:forEach>
	                </select>
	                <span class="cp_sl06_highlight"></span>
	                <span class="cp_sl06_selectbar"></span>
	                <label class="cp_sl06_selectlabel">種類</label>
            	</div>
				<div class="cp_iptxt">
					<input class="ef" type="text" name="name" value="${idb.name}">
					<label>商品名</label> <span class="focus_line"></span>
				</div>
				<div class="cp_iptxt">
					<input class="ef" type="text" name="price" value="${idb.price}">
					<label>値段</label> <span class="focus_line"></span>
				</div>
				<div class="cp_iptxt">
					<input class="ef" type="text" name="detail" value="${idb.detail}">
					<label>商品詳細</label> <span class="focus_line"></span>
				</div>

				<span class="btn-haichi2">
					<div class="col s12">
						<div class="btn-modoru">
							<p class="center-align">
								<button class="btn-square-pop" type="submit" name="?">戻る</button>
							</p>
						</div>
					</div>
					<div class="col s12">
						<div class="btn-touroku">

							<p class="center-align">
								<button class="btn-square-pop" type="submit">登録</button>
							</p>
						</div>
					</div>
				</span>
			</div>
		</form>


		<footer>
			<nav id="footer-nav">
        <ul>
            <li class=”current”><a href="SearchByCategory?item_detail_id=1" name="item_detail_id">Tops</a></li>
            <li><a href="SearchByCategory?item_detail_id=2" name="item_detail_id">Pants</a></li>
            <li><a href="SearchByCategory?item_detail_id=3" name="item_detail_id">Outerwear</a></li>
            <li><a href="SearchByCategory?item_detail_id=4" name="item_detail_id">Shoes</a></li>
            <li><a href="SearchByCategory?item_detail_id=5" name="item_detail_id">Other</a></li>
        </ul>
			</nav>
			<p>© All rights reserved by Fashion EC.</p>
		</footer>

	</div>




</body>

</html>
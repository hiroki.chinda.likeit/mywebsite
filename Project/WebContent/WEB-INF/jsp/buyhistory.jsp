<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta charset="UTF-8">
<title>購入履歴画面</title>
<link href="css/style.css" rel="styleSheet" type="text/css" />
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
	integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
	crossorigin="anonymous">
<!-- Place your kit's code here -->
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css"
	rel="stylesheet">
</head>

<body>
	<div class="wrapper">
		<header>
			<h1 class="headline">
				<a href="Index" style="text-decoration: none;" class="title">Fashion
					EC</a>
			</h1>
			<ul class="nav-list">
				<li class="nav-list-item"><a href="UserData"><i
						class="fas fa-user">ユーザ情報</i></a></li>
				<li class="nav-list-item"><a href="Cart"><i
						class="fas fa-shopping-cart">カート</i></a></li>
				<li class="nav-list-item"><a href="Logout"><i
						class="fas fa-sign-out-alt">ログアウト</i></a></li>
			</ul>
		</header>

		<p class="sample_h_4">購入履歴</p>


		<div class="cp_actab">
			<c:forEach var="resultBDB" items="${resultBDBList}">
				<input id="tab-one" type="checkbox" name="tabs">
				<label for="tab-one">
					<table class="buyhistory-table">
						<tr>
							<th>購入日時 : ${resultBDB.createDate}</th>
							<th>購入金額合計 : ${resultBDB.formatTotalPrice}円</th>
						</tr>
					</table>
				</label>
				<div class="cp_actab-content">
					<div class="table-buy">
						<table>
							<tr>
								<th>商品名</th>
								<th>値段</th>
								<th>小計</th>
							</tr>
							<c:forEach var="buyIDB" items="${buyIDBList}">
								<c:if test="${resultBDB.id == buyIDB.buyId }">
									<tr>
										<td>${buyIDB.name}</td>
										<td>${buyIDB.formatPrice}円</td>
										<td>${buyIDB.formatPrice}円</td>
									</tr>
								</c:if>
							</c:forEach>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<th>配送方法</th>
								<td>${resultBDB.deliveryMethodName}</td>
								<td>${resultBDB.deliveryMethodPrice}円</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<th>合計金額</th>
								<td></td>
								<td>${resultBDB.formatTotalPrice}円</td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<th>決済方法</th>
								<td></td>
								<td>${resultBDB.paymentMethodName}</td>
							</tr>
						</table>
					</div>
				</div>
			</c:forEach>
		</div>






		<footer>
			<nav id="footer-nav">
        <ul>
            <li class=”current”><a href="SearchByCategory?item_detail_id=1" name="item_detail_id">Tops</a></li>
            <li><a href="SearchByCategory?item_detail_id=2" name="item_detail_id">Pants</a></li>
            <li><a href="SearchByCategory?item_detail_id=3" name="item_detail_id">Outerwear</a></li>
            <li><a href="SearchByCategory?item_detail_id=4" name="item_detail_id">Shoes</a></li>
            <li><a href="SearchByCategory?item_detail_id=5" name="item_detail_id">Other</a></li>
        </ul>
			</nav>
			<p>© All rights reserved by Fashion EC.</p>
		</footer>

	</div>




</body>

</html>